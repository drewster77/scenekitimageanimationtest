//
//  ViewController.swift
//  CALayerKeyFrameImageAnimTest
//
//  Created by Andrew Benson on 4/9/18.
//  Copyright © 2018 Nuclear Cyborg Corp. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {

    @IBOutlet weak var sceneView: SCNView!
    
    @IBAction func buttonWasTapped(_ sender: Any) {
        
        animationImageFrameIndex = 0
        displayLink = CADisplayLink(target: self, selector: #selector(animationStep(_:)))
        displayLink!.preferredFramesPerSecond = 60
        displayLink!.add(to: .current, forMode: .defaultRunLoopMode)
    }
    
    @objc func animationStep(_ displayLink: CADisplayLink) {
        let desiredFPS : Double = 24
        let realFPS = 1 / (displayLink.targetTimestamp - displayLink.timestamp)
        
        material.diffuse.contents = images[Int(animationImageFrameIndex)]
        animationImageFrameIndex += desiredFPS / realFPS
        if Int(animationImageFrameIndex) >= images.count {
            displayLink.remove(from: .current, forMode: .defaultRunLoopMode)
        }
    }
    let imageFileNameFormat = "heart.%04d.png"
    var images : [UIImage] = []
    var planeNode = SCNNode()
    var material = SCNMaterial()
    var animationImageFrameIndex : Double = 0
    var displayLink : CADisplayLink?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        for i in 1 ... 26 {
            let filename = String(format: imageFileNameFormat, i)
            images.append(UIImage(named: filename)!)
        }
      
        let geo = SCNPlane(width: 1, height: 1  )
        geo.widthSegmentCount = 20
        geo.heightSegmentCount = 20
        planeNode.geometry = geo

        material.diffuse.contents = UIColor.red
        material.isDoubleSided = false
        planeNode.geometry?.firstMaterial = material
        
        let camNode = SCNNode()
        camNode.position = SCNVector3Make(0, 0, 1.5)
        camNode.camera = SCNCamera()

        let scene = SCNScene()
        scene.rootNode.addChildNode(camNode)
        scene.rootNode.addChildNode(planeNode)
        
        sceneView.scene = scene
        sceneView.pointOfView = camNode
        sceneView.autoenablesDefaultLighting = true
        sceneView.allowsCameraControl = true
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

